FROM python:alpine3.7

WORKDIR /

RUN pip install elasticsearch-curator

COPY entrypoint.py curator.yml /
COPY action_files /action_files

CMD ["python", "/entrypoint.py"]
