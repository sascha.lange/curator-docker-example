# Description

Simple example how to build a docker container that executes curator policies. The entrypoint.py script uses the curotar.yml as basic configuration and executes all action_files in the actions_file directory.

The actions_files directory containes a example file that will delete all indecies that are older than 30 days, based on the creation date of the index. It excludes the kibana index itself.

## Prerequisite

***You need to add a valid elasticsearch url in the curator.yml configuration!***

## Build instructions

Both build examples will create a docker image with the name curator.

### docker build

```shell
docker build --no-cache -t curator .
```

## make

```shell
make clean && make
```
