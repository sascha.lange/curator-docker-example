.PHONY: build clean

build:
	docker build -t curator-example .
clean:
	docker image rm -f curator-example