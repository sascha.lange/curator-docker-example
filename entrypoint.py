#!/usr/bin/env python

import os

from subprocess import call

def run_curator(config_file, action_file_dir):
    for f in os.listdir(action_file_dir):
        command = [ "curator",
                    "--config",
                    config_file,
                    action_file_dir + "/" + f ]

        print("execute action_file %s" % (f))
        call(command)

if __name__ == "__main__":
    run_curator('curator.yml', '/action_files')
